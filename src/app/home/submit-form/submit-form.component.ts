import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.css']
})
export class SubmitFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Output() emitAddArticle = new EventEmitter<{title:string, link:string}>();

  addNewArticleClick(newTitle:HTMLInputElement, newLink:HTMLInputElement){
    this.emitAddArticle.emit({title: newTitle.value, link: newLink.value});

    newTitle.value = '';
    newLink.value = '';
  }
}
