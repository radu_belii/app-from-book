import {
  Component,
  OnInit,
  HostBinding,
  Input
} from '@angular/core';
import { Article } from '../../models/article/article.model'

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'noidar';
  @Input() article: Article;

  constructor() {

  }
  voteUp(): boolean {
    this.article.voteUp();
    return false;
  }

  voteDown() {
    this.article.voteDown();
    return false;
  }

  ngOnInit() {
  }

}


