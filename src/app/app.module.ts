import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './shared/layout/material.module'

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonComponent } from './shared/button/button.component';
import { SubmitFormComponent } from './home/submit-form/submit-form.component';
import { ArticleComponent } from './home/article/article.component';


@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    SubmitFormComponent,
    ArticleComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
