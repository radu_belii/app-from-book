import { Component } from '@angular/core';
import { Article } from './models/article/article.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app-from-book';
  articles: Article[]

  constructor() {
    this.articles = [
      new Article('Angular 2', 'http://angular.io', 3),
      new Article('Fullstack', 'http://fullstack.io', 2),
      new Article('Angular Homepage', 'http://angular.io', 1),
    ];
  }

  addArticle = (event:{title:string, link: string}): boolean => {
    this.articles.push(new Article(event.title, event.link));
    return false;
  }

  sortByRating(): Article[] {
    return  this.articles.sort( (a: Article, b: Article) => b.votes -a.votes )
  }

}
